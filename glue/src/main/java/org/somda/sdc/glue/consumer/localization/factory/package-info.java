/**
 * Factories for the Localization service consumer side.
 */
@ParametersAreNonnullByDefault
package org.somda.sdc.glue.consumer.localization.factory;

import javax.annotation.ParametersAreNonnullByDefault;