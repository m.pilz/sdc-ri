/**
 * Implementation of localization service consumer.
 */
@ParametersAreNonnullByDefault
package org.somda.sdc.glue.consumer.localization;

import javax.annotation.ParametersAreNonnullByDefault;