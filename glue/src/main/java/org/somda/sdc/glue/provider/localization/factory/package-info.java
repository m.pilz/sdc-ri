/**
 * Factories for the SDC provider side.
 */
@ParametersAreNonnullByDefault
package org.somda.sdc.glue.provider.localization.factory;

import javax.annotation.ParametersAreNonnullByDefault;