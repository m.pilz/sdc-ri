/**
 * Factories for the SDC provider side.
 */
@ParametersAreNonnullByDefault
package org.somda.sdc.glue.provider.localization.helper;

import javax.annotation.ParametersAreNonnullByDefault;