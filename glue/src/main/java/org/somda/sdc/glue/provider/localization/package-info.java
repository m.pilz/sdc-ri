/**
 * Implementation of localization service provider.
 */
@ParametersAreNonnullByDefault
package org.somda.sdc.glue.provider.localization;

import javax.annotation.ParametersAreNonnullByDefault;